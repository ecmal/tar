export class Utils {
    static LOOKUP =[
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
        'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '/'
    ];
    static clean(length) {
        var i, buffer = new Uint8Array(length);
        for (i = 0; i < length; i += 1) {
            buffer[i] = 0;
        }
        return buffer;
    }

    static extend(orig, length, addLength, multipleOf) {
        var newSize = length + addLength,
            buffer = this.clean((((newSize / multipleOf)|0) + 1) * multipleOf);

        buffer.set(orig);

        return buffer;
    }

    static pad(num, bytes, base?) {
        num = num.toString(base || 8);
        return "000000000000".substr(num.length + 12 - bytes) + num;
    }

    static stringToUint8(input, out?, offset?) {
        var i, length;

        out = out || this.clean(input.length);

        offset = offset || 0;
        for (i = 0, length = input.length; i < length; i += 1) {

            out[offset] = input.charCodeAt(i);
            offset += 1;
        }

        return out;
    }

    static uint8ToBase64(uint8) {
        var i,
            extraBytes = uint8.length % 3, // if we have 1 byte left, pad 2 bytes
            output = "",
            temp, length;

        function tripletToBase64(num) {
            return Utils.LOOKUP[num >> 18 & 0x3F] + Utils.LOOKUP[num >> 12 & 0x3F] + Utils.LOOKUP[num >> 6 & 0x3F] + Utils.LOOKUP[num & 0x3F];
        }

        // go through the array every three bytes, we'll deal with trailing stuff later
        for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
            temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2]);
            output += tripletToBase64(temp);
        }

        // this prevents an ERR_INVALID_URL in Chrome (Firefox okay)
        switch (output.length % 4) {
            case 1:
                output += '=';
                break;
            case 2:
                output += '==';
                break;
            default:
                break;
        }

        return output;
    }
}
export class Tar {

    static RECORD_SIZE = 512;
    static RECORD_PER_BLOCK = 20;
    static HEADERS = [
        {
            'field': 'fileName',
            'length': 100
        },
        {
            'field': 'fileMode',
            'length': 8
        },
        {
            'field': 'uid',
            'length': 8
        },
        {
            'field': 'gid',
            'length': 8
        },
        {
            'field': 'fileSize',
            'length': 12
        },
        {
            'field': 'mtime',
            'length': 12
        },
        {
            'field': 'checksum',
            'length': 8
        },
        {
            'field': 'type',
            'length': 1
        },
        {
            'field': 'linkName',
            'length': 100
        },
        {
            'field': 'ustar',
            'length': 8
        },
        {
            'field': 'owner',
            'length': 32
        },
        {
            'field': 'group',
            'length': 32
        },
        {
            'field': 'majorNumber',
            'length': 8
        },
        {
            'field': 'minorNumber',
            'length': 8
        },
        {
            'field': 'filenamePrefix',
            'length': 155
        },
        {
            'field': 'padding',
            'length': 12
        }
    ];
    static header(data, cb?) {
        var buffer = Utils.clean(512),
            offset = 0;
        Tar.HEADERS.forEach((value)=>{
            var str = data[value.field] || "", i, length;
            for (i = 0, length = str.length; i < length; i += 1) {
                buffer[offset] = str.charCodeAt(i);
                offset += 1;
            }
            offset += value.length - i; // space it out with nulls
        });
        if (typeof cb === 'function') {
            return cb(buffer, offset);
        }
        return buffer;
    }

    private written;
    private blockSize;
    public out;

    constructor(recordsPerBlock?) {
        this.written = 0;
        this.blockSize = (recordsPerBlock || Tar.RECORD_PER_BLOCK) * Tar.RECORD_SIZE;
        this.out = Utils.clean(this.blockSize);
    }

    append(filepath, input, opts?, callback?) {
        var data, checksum, mode, mtime, uid, gid, headerArr;
        if (typeof input === 'string') {
            input = Utils.stringToUint8(input);
        } else
        if (input.constructor !== Uint8Array.prototype.constructor) {
            throw 'Invalid input type. You gave me: ' + input.constructor.name;
        }
        if (typeof opts === 'function') {
            callback = opts;
            opts = {};
        }
        opts = opts || {};
        mode = opts.mode || parseInt('777', 8) & 0xfff;
        mtime = opts.mtime || Math.floor(+new Date() / 1000);
        uid = opts.uid || 0;
        gid = opts.gid || 0;
        data = {
            fileName    : filepath,
            fileMode    : Utils.pad(mode, 7),
            uid         : Utils.pad(uid, 7),
            gid         : Utils.pad(gid, 7),
            fileSize    : Utils.pad(input.length, 11),
            mtime       : Utils.pad(mtime, 11),
            checksum    : '        ',
            type        : '0', // just a file
            ustar       : 'ustar  ',
            owner       : opts.owner || '',
            group       : opts.group || ''
        };

        // calculate the checksum
        checksum = 0;
        Object.keys(data).forEach((key)=>{
            var i, value = data[key], length;
            for (i = 0, length = value.length; i < length; i += 1) {
                checksum += value.charCodeAt(i);
            }
        });

        data.checksum = Utils.pad(checksum, 6) + "\u0000 ";
        console.info(JSON.stringify(data));
        headerArr = Tar.header(data);

        var i, offset, length;

        this.out.set(headerArr, this.written);

        this.written += headerArr.length;

        // If there is not enough space in this.out, we need to expand it to
        // fit the new input.
        if (this.written + input.length > this.out.length) {
            this.out = Utils.extend(this.out, this.written, input.length, this.blockSize);
        }

        this.out.set(input, this.written);

        // to the nearest multiple of Tar.RECORD_SIZE
        this.written += input.length + (Tar.RECORD_SIZE - (input.length % Tar.RECORD_SIZE || Tar.RECORD_SIZE));

        // make sure there's at least 2 empty records worth of extra space
        if (this.out.length - this.written < Tar.RECORD_SIZE * 2) {
            this.out = Utils.extend(this.out, this.written, Tar.RECORD_SIZE * 2, this.blockSize);
        }

        if (typeof callback === 'function') {
            callback(this.out);
        }

        return this.out;
    }
    clear(){
        this.written = 0;
        this.out = Utils.clean(this.blockSize);
    }
}
export class Untar {
    private buffer:Buffer;
    private offset:number;
    constructor(buffer){
        this.offset=0;
        this.buffer=buffer;
    }
    get hasNext(){
        return this.offset<this.buffer.length;
    }
    next():any{
        if(this.hasNext) {
            var content, headers:any = {};
            var offset = this.offset;

            Tar.HEADERS.forEach(h=> {
                var key = h.field;
                var len = h.length;
                var val = this.buffer.toString('utf8', offset, offset + len);
                headers[key] = val.substring(0, val.indexOf('\u0000'));
                offset += len;
            });
            if(headers.checksum){
                this.offset += 512;
                headers.fileSize = parseInt(headers.fileSize,8);
                content = this.buffer.toString('utf8',this.offset, this.offset + headers.fileSize);
                this.offset += Math.ceil(headers.fileSize / 512) * 512;
                if(this.buffer[this.offset+148]==0){
                    this.offset = this.buffer.length;
                }
                return {headers, content};
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
}