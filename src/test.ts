import * as Fs from 'node/fs';
import * as Zlib from 'node/zlib';

import {Tar} from "./tar";
import {Untar} from "./tar";


var tar = new Tar();

var content = 'Hello World';

console.info(content.length);
tar.append('output.txt',  content);
tar.append('dir/out.txt', content+content);

var entry,untar = new Untar(new Buffer( tar.out ));
while(untar.hasNext){
    console.info(untar.next())
}

//Fs.writeFileSync('test.tar',new Buffer( tar.out ));
Fs.writeFileSync('test.tgz',Zlib.gzipSync(new Buffer( tar.out )));